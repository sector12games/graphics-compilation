A lot of Buuf stuff is created specifically for use with icons on mac computers (.icns files).
Several of his packages (super buff, buff deuce) didn't provide the pngs. I had to extract them
manually using "IcoFx". This was a major pain in the butt because the free version only
allows you to extract 5 icons at a time. In addition, the trial version times out after a few
weeks or a month. Also, the extraction process caused distortion on some of the smaller images (32x32). 
The largest image of the icon always seemed to be fine though.

How to extract .icns files:
Open IcoFx
 -> Tools -> Batch Create Images...


Shaun 7/15/2013