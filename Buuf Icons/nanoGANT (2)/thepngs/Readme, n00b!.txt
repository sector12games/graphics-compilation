This is G.A.N.T a set derived from the hatred of the mechanical way in which the Umicons where produced and the redundancy thereof. It was all point and click(the Polygonol Lasso tool) accompanied by the occasional drag(Gradient Tool). After two months and three icons a day, at least, it got VERY monotonous. How is this any different you ask? well, as indicated by the file in this directory, I just got a new tablet, and in my eagerness to test it out I took the lineart from the monkey in Umicons Volume 3 and painted it, deeply satisfied I decided to make it into an icon to be included in the Umicons Volume 4 \Extra subfolder. Later on I got carried away and did a bunch of other icons in the G.A.N.T style until it was way too much for me to include in volume 4 as a subset. 

"Anyway, long story short... is a phrase whose origins are complicated and rambling" -Abraham Simpson   

As I was saying... um... I don't remember...and I don't feel like scrolling upward to find out...so..um... basically G.A.N.T is a spinoff of Umicons. And of course by 'spinoff of' I mean replacement for Umicons. Yes, you read right, Volume 4 will be the last, all good things must come to an end at some point, on the other hand so must the Umicon Volumes. J/K, Umicons will be back...someday.

-----2-----

Why do you see folders?

Most of the system icons were covered in ver. 1 and with the new graphite folder, the "Folders" subfolder became the largest so it naturally became the root folder. I did the same thing for one of the Umicon Volumes, except it was the "Misc" subdirectory.  

I've come to realise that the original GANT set was a little daunting to a lot of potential users, so I've included an Install.txt, read it over if you're a n00b. Of course, If your not a n00b then you wouldn't need to read, nor would you need me to tell you that...thereby making this sentence completely pointless. 
 