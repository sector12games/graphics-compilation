# -------------------------------------------------------------- #
This is a bundle pack containing Image Based resources created/edited by IceDragon.

If you didn't download this pack from RPGVX.net please contact me immediately with the link to the website you downloaded it from.
mistdragon100@yahoo.com

Credit the following people if you use the resources in this pack.

There has been a change to the bundle pack as you have noticed.
All resources have been removed from there version folders and have been simply placed in there type-folders.
In addition small credit lists may be found in all the folders
# -------------------------------------------------------------- #

Avadan
Ccoa
Celianna
Enterbrain
Hanzo Kimura
Kaduki
Konoe - http://konoee.blog137.fc2.com/blog-entry-23.html
Mack
IceDragon
Lunarea
Terra-Chan
Sam Green
Sen
Shin 
Weem
Whitecat
Ying

# -------------------------------------------------------------- #
All resources in this pack are free to use.
You may edit them and redistribute them.

All I ask is that you credit me (IceDragon) and the respective individuals mentioned in the credits

You may see a different name for me.
Some alternatives are.

IceDragon
IceDragon200
IcyDragon
IcyDragon200

Please enjoy these resources. And remember CREDIT GOOD RESOURCES!

Also note the .ico files are not mine...

# -------------------------------------------------------------- #
IRBP V1.4
Date Updated 01/27/2011